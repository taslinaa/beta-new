const userInfo = document.querySelector('#user-info')
const editBtn = document.querySelector('#edit')
const deleteBtn = document.querySelector('#delete')
const saveBtn = document.querySelector('#save')
const cancelBtn = document.querySelector('#cancel')
const form = document.querySelector('form')

editBtn.addEventListener('click', () => {
  userInfo.classList.add('d-none')
  form.classList.remove('d-none')
  editBtn.classList.add('d-none')
  deleteBtn.classList.add('d-none')
})

cancelBtn.addEventListener('click', () => {
  location.reload()
})

saveBtn.addEventListener('click', (ev) => {
  ev.preventDefault()
  const oldUsername = document.getElementById('username').textContent.trim()
  const messageError = document.querySelector('.message.error')

  // get value
  const about = form.about.value
  const email = form.email.value
  const username = form.username.value
  const phone = form.phone.value
  const gender = form.gender.value

  const headers = new Headers()
  headers.append('Content-Type', 'application/json')

  const body = JSON.stringify({
    about, email, username, phone, gender,
  })

  const requestOptions = {
    method: 'PATCH',
    headers,
    body,
    redirect: 'follow',
  }

  const URL = `http://localhost:3000/api/user/${oldUsername}`

  fetch(URL, requestOptions)
    .then((response) => response.json())
    .then((result) => {
      console.log(result)
      if (result.success) {
        const alert = document.querySelector('.alert')
        alert.classList.add('show')

        setInterval(() => {
          location.pathname = `/user/${username}/profile`
        }, 1500);
      }

      if (result.errors) {
        messageError.textContent = result.errors
      }
    })
    .catch((error) => console.log('error', error))
})
