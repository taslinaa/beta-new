/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
const baseUrlUser = 'http://localhost:3000/api/users'

const nextBtnUser = document.getElementById('next-user')
const prevBtnUser = document.getElementById('prev-user')
const tbodyUser = document.getElementById('list-user')

const minPagesUser = 1
let maxPagesUser = 0
let currentPageUser = 1

const reqOptUser = {
  method: 'GET',
}

const getUser = (url) => {
  fetch(url, reqOptUser)
    .then((response) => response.json())
    .then((result) => {
      console.log(result)
      const { maxPage, users } = result
      maxPagesUser = maxPage

      createTableRowUser(users)
      changeStateBtnUser()
    })
    .catch((error) => console.log('error', error))
}

// call when first load page
getUser(baseUrlUser)

const createTableRowUser = (users) => {
  users.forEach((user) => {
    const tableRow = document.createElement('tr')
    tableRow.classList.add('user-row')
    const { biodata } = user
    tableRow.innerHTML = `
      <td>${user.username}</td>
      <td>${biodata.email}</td>
      <td>${biodata.phone}</td>
      <td>${user.role}</td>
      <td>${biodata.gender}</td>
      `
    tbodyUser.appendChild(tableRow)
  })
}

const changeStateBtnUser = () => {
  if (currentPageUser === minPagesUser) {
    prevBtnUser.disabled = true
  } else {
    prevBtnUser.disabled = false
  }

  if (currentPageUser === maxPagesUser) {
    nextBtnUser.disabled = true
  } else {
    nextBtnUser.disabled = false
  }
}

const replaceTableRowUser = () => {
  const oldChild = tbodyUser.querySelectorAll('.user-row')
  oldChild.forEach((el) => {
    tbodyUser.removeChild(el)
  })
}

nextBtnUser.addEventListener('click', () => {
  currentPageUser++
  console.log(currentPageUser)
  const query = `?page=${currentPageUser}`
  const newUrl = baseUrlUser + query

  replaceTableRowUser()
  getUser(newUrl)
})

prevBtnUser.addEventListener('click', () => {
  currentPageUser--
  console.log(currentPageUser)
  const query = `?page=${currentPageUser}`
  const newUrl = baseUrlUser + query

  replaceTableRowUser()
  getUser(newUrl)
})
