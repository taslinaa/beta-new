/* eslint-disable no-use-before-define */
/* eslint-disable no-undef */
const baseUrlHistory = 'http://localhost:3000/api/users/history'

const nextBtnHistory = document.getElementById('next-history')
const prevBtnHistory = document.getElementById('prev-history')
const tbodyHistory = document.getElementById('list-history')

const minPagesHistory = 1
let maxPagesHistory = 0
let currentPageHistory = 1

const getHistory = (url) => {
  const reqOptHistory = {
    method: 'GET',
  }

  fetch(url, reqOptHistory)
    .then((response) => response.json())
    .then((result) => {
      console.log(result)
      const { histories, maxPage } = result
      maxPagesHistory = maxPage

      createTableRowHistory(histories)
      changeStateBtnHistory()
    })
    .catch((error) => console.log('error', error))
}

// call when first load page
getHistory(baseUrlHistory)

const createTableRowHistory = (histories) => {
  histories.forEach((history) => {
    const tableRow = document.createElement('tr')
    tableRow.classList.add('history-row')
    tableRow.innerHTML = `
    <td>${history.playerOne}</td>
    <td>${history.playerOnePicked}</td>
    <td>${history.playerTwo}</td>
    <td>${history.playerTwoPicked}</td>
    <td>${history.result}</td>
    `
    tbodyHistory.appendChild(tableRow)
  })
}

const changeStateBtnHistory = () => {
  if (currentPageHistory === minPagesHistory) {
    prevBtnHistory.disabled = true
  } else {
    prevBtnHistory.disabled = false
  }

  if (currentPageHistory === maxPagesHistory) {
    nextBtnHistory.disabled = true
  } else {
    nextBtnHistory.disabled = false
  }
}

const replaceTableRowHistory = () => {
  const oldChild = tbodyHistory.querySelectorAll('.history-row')
  oldChild.forEach((el) => {
    tbodyHistory.removeChild(el)
  })
}

nextBtnHistory.addEventListener('click', () => {
  currentPageHistory++
  console.log(currentPageHistory)
  const query = `?page=${currentPageHistory}`
  const newUrl = baseUrlHistory + query

  replaceTableRowHistory()
  getHistory(newUrl)
})

prevBtnHistory.addEventListener('click', () => {
  currentPageHistory--
  console.log(currentPageHistory)
  const query = `?page=${currentPageHistory}`
  const newUrl = baseUrlHistory + query

  replaceTableRowHistory()
  getHistory(newUrl)
})
