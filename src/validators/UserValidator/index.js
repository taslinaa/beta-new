import { body } from 'express-validator'

export default class UserValidator {
  static message = {
    email: 'please input a valid email',
    username: 'username cannot be blank',
    password: 'password cannot be blank',
  }

  static register = [
    body('username').exists().isString().notEmpty()
      .withMessage(this.message.username),
    body('password').exists().isString().notEmpty()
      .withMessage(this.message.password),
    body('email').exists().isEmail()
      .withMessage(this.message.email),
    body('gender').exists().isWhitelisted(['m', 'f']),
  ]

  static login = [
    body('username').exists().isString().notEmpty()
      .withMessage(this.message.username),
    body('password').exists().isString().notEmpty()
      .withMessage(this.message.password),
  ]

  static update = [
    body('username').exists().isString().notEmpty()
      .withMessage(this.message.username),
    body('email').exists().isEmail()
      .withMessage(this.message.email),
    body('gender').exists().isWhitelisted(['m', 'f']),
    body('phone').optional(),
    body('about').optional(),
  ]
}
