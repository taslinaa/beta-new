const { biodata } = require('../dataseeders')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserBiodata', biodata, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserBiodata', null, {});
  },
}
