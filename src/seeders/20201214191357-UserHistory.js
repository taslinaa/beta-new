const { userHistory } = require('../dataseeders')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('UserHistory', userHistory, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('UserHistory', null, {});
  },
}
