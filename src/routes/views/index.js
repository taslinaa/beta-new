import { Router } from 'express'
import ViewController from '../../controllers/viewController'
import Middlewares from '../../middlewares'

const router = Router()

router.get('/home', ViewController.home)
router.get('/dashboard', Middlewares.Admin, ViewController.dashboard)
router.get('/user/login', ViewController.login)
router.get('/user/register', ViewController.register)
router.get('/user/:username/profile', Middlewares.Auth, ViewController.profile)
router.get('/game/play/:roomCode', Middlewares.Auth, ViewController.game)
router.get('/game/lobby', Middlewares.Auth, ViewController.lobby)

export default router
