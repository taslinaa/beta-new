import { UserGame, GameRoom } from '../../models'

export default class ViewController {
  static home = (req, res) => res.render('home')

  static dashboard = (req, res) => res.render('dashboard')

  static login = (req, res) => res.render('login')

  static register = (req, res) => res.render('register')

  static profile = async (req, res) => {
    const { username } = req.params

    try {
      const user = await UserGame.findOne({ where: { username } })

      if (!user) return res.status(404).render('404')

      return res.render('profile')
    } catch (error) {
      return res.status(500).json({ errors: error.message })
    }
  }

  static lobby = (req, res) => res.render('lobby')

  static game = async (req, res) => {
    const { roomCode } = req.params

    try {
      const room = await GameRoom.findOne({ where: { roomCode } })

      if (!room) return res.status(404).json({ errors: 'failed enter room', message: 'room not found' })

      return res.status(200).render('game', { room })
    } catch (error) {
      console.log('game view controller :', error)
      return res.status(400).json({ errors: error.message })
    }
  }
}
