module.exports.ENUMS = {
  userPick: ['rock', 'paper', 'scissors'],
  resultGame: ['player one win', 'player two win', 'draw'],
  resultHistoryGame: ['win', 'lose', 'draw'],
  gender: ['m', 'f'],
  role: ['admin', 'user'],
}
