const { ENUMS } = require('../utils/ENUM')

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('GameRoom', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      roomCode: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      playerOne: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      playerTwo: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      playerOnePicked: {
        type: Sequelize.ENUM,
        values: ENUMS.userPick,
      },
      playerTwoPicked: {
        type: Sequelize.ENUM,
        values: ENUMS.userPick,
      },
      result: {
        type: Sequelize.ENUM,
        values: ENUMS.resultGame,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('GameRoom')
  },
}
