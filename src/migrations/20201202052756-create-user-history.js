module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('UserHistory', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        unique: true,
      },
      result: {
        type: Sequelize.ENUM,
        values: ['won', 'lose', 'draw'],
      },
      userPicked: {
        type: Sequelize.ENUM,
        values: ['rock', 'paper', 'scissors'],
      },
      opponentsPicked: {
        type: Sequelize.ENUM,
        values: ['rock', 'paper', 'scissors'],
      },
      userId: {
        type: Sequelize.UUID,
        allowNull: false,
        onDelete: 'cascade',
        references: {
          model: 'UserGame',
          key: 'id',
        },
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('UserHistory')
  },
}
