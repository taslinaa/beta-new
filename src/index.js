import express from 'express'
import logger from 'morgan'
import path from 'path'
import cookieParser from 'cookie-parser'
import cors from 'cors'
import yaml from 'yamljs'
import swaggerUi from 'swagger-ui-express'
import viewRoute from './routes/views'
import apiRoute from './routes/apis'
import { checkUser } from './middlewares/Auth'
import { sequelize } from './models'

const app = express()
const PORT = process.env.PORT || 3100

// view engine
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

// dbconnection
sequelize.authenticate()
  .then(() => console.log('Connection successfully'))
  .catch((err) => {
    console.log('failed', err.message)
  })

// base middleware
app.use(cors())
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, '../public')))
app.use(cookieParser())

// routes
app.get('*', checkUser)
app.get('/', (req, res) => res.redirect('/home'))
app.use(viewRoute)
app.use('/api', apiRoute)
const swaggerDoc = yaml.load(path.resolve(__dirname, '../openapi.yaml'))
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc))

// catch 404 and forward to error handler
app.use((req, res, next) => res.status(404).render('404'))

// error handler middleware
app.use((error, req, res, next) => {
  console.log(error)
  res.status(error.status || 500).render('500')
})

app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`))
