const { Model } = require('sequelize')
const { compare } = require('bcrypt')
const { hashing } = require('../utils')
const { ENUMS } = require('../utils/ENUM')

module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    static associate(models) {
      const { UserBiodata, UserHistory } = models

      UserGame.hasOne(UserBiodata, {
        foreignKey: 'userId',
        as: 'biodata',
      })

      UserGame.hasMany(UserHistory, {
        foreignKey: 'userId',
        as: 'history',
      })
    }

    static login = async (username, password) => {
      const user = await UserGame.findOne({ where: { username } })

      if (user) {
        const auth = await compare(password, user.password)
        if (auth) return user

        throw Error('incorrect password')
      }

      throw Error('incorrect username')
    }
  }

  UserGame.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: {
          msg: 'username has been used',
        },
        validate: {
          notEmpty: {
            msg: 'username cannot blank',
          },
          notNull: {
            msg: 'username cannot be null',
          },
          len: {
            msg: 'insert username at least 4 characters',
            args: [4, 30],
          },
        },
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
          notEmpty: {
            msg: 'password cannot blank',
          },
          len: {
            msg: 'insert password at least 6 characters',
            args: [6, 64],
          },
          notNull: {
            msg: 'password cannot be null',
          },
        },
      },
      role: {
        type: DataTypes.ENUM,
        values: ENUMS.role,
        defaultValue: 'user',
      },
    },
    {
      sequelize,
      modelName: 'UserGame',
    },
  )

  UserGame.beforeBulkCreate((users, op) => {
    op.individualHooks = true

    users.forEach((user) => hashing(user))
  })

  UserGame.beforeCreate((user) => hashing(user))

  return UserGame
}
