const { Model } = require('sequelize')
const { ENUMS } = require('../utils/ENUM')

module.exports = (sequelize, DataTypes) => {
  class GameRoom extends Model {
    static associate(models) { }
  }

  GameRoom.init({
    roomCode: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
    },
    playerOne: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
    },
    playerTwo: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: '',
    },
    playerOnePicked: {
      type: DataTypes.ENUM,
      values: ENUMS.userPick,
    },
    playerTwoPicked: {
      type: DataTypes.ENUM,
      values: ENUMS.userPick,
    },
    result: {
      type: DataTypes.ENUM,
      values: ENUMS.resultGame,
    },
  }, {
    sequelize,
    modelName: 'GameRoom',
  })
  return GameRoom;
}
