const { Model } = require('sequelize')
const { ENUMS } = require('../utils/ENUM')

module.exports = (sequelize, DataTypes) => {
  class History extends Model {
    static associate(models) { }
  }

  History.init({
    playerOne: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    playerTwo: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    playerOnePicked: {
      type: DataTypes.ENUM,
      values: ENUMS.userPick,
      allowNull: false,
    },
    playerTwoPicked: {
      type: DataTypes.ENUM,
      values: ENUMS.userPick,
      allowNull: false,
    },
    result: {
      type: DataTypes.ENUM,
      values: ENUMS.resultGame,
      allowNull: false,
    },
  }, {
    sequelize,
    modelName: 'History',
  })
  return History
}
