const { Model } = require('sequelize')
const { ENUMS } = require('../utils/ENUM')

module.exports = (sequelize, DataTypes) => {
  class UserHistory extends Model {
    static associate(models) {
      const { UserGame } = models

      UserHistory.belongsTo(UserGame, {
        foreignKey: 'userId',
        as: 'user',
      })
    }
  }
  UserHistory.init(
    {
      id: {
        primaryKey: true,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      result: {
        type: DataTypes.ENUM,
        values: ENUMS.resultHistoryGame,
      },
      userPicked: {
        type: DataTypes.ENUM,
        values: ENUMS.userPick,
      },
      opponentsPicked: {
        type: DataTypes.ENUM,
        values: ENUMS.userPick,
      },
      userId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
    },
    {
      sequelize,
      modelName: 'UserHistory',
    },
  )
  return UserHistory
}
